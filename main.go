package main

import (
	"flag"
	"fmt"

	"github.com/bwmarrin/discordgo"
)

var (
	Token string
)

func init() {
	flag.StringVar(&Token, "t", "", "Bot Token")
	flag.Parse()
}

func main() {
	dg, err := discordgo.New(Token)
	if err != nil {
		fmt.Println("not good token")
	}
	err = dg.Open()
	if err != nil {
		fmt.Println("cant open session")
	}
}
